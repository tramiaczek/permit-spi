# Installation of Keycloak extension
* Compilation and packaging: `mvn clean package`

* Start Keycloak:
```bash
    cd $KEYCLOAK_HOME
    ./standalone.sh(*.bat)
```

* Install using CLI:
```bash
    cd $KEYCLOAK_HOME/bin
    jboss-cli.sh (*.bat) --command="module add --name=com.onwelo.extensions.keycloak.permit-spi --resources=permit-spi\target\permits-spi-1.0-SNAPSHOT.jar --dependencies=org.keycloak.keycloak-core,org.keycloak.keycloak-services,org.keycloak.keycloak-model-jpa,org.keycloak.keycloak-server-spi,org.keycloak.keycloak-server-spi-private,javax.ws.rs.api,org.apache.commons.logging
```
    
* Modify /auth web-context in $KEYCLOAK_HOME/conf/standalone.xml:
```xml
    <subsystem xmlns="urn:jboss:domain:keycloak-server:1.1">
        <web-context>auth</web-context>
        <providers>
            <provider>classpath:${jboss.home.dir}/providers/*</provider>  
			<provider>module:com.onwelo.extensions.keycloak.permit-spi</provider>  
        </providers>
            ...
    </subsystem>
```

* REST endpoints require Authorization: Bearer token with JWT for end user and are available under:
```bash
   http://localhost:8080/auth/realms/<realm>/permits/
```