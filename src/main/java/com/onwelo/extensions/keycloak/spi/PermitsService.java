package com.onwelo.extensions.keycloak.spi;

import com.onwelo.extensions.keycloak.rest.dto.PermitResponse;
import org.keycloak.models.UserModel;
import org.keycloak.provider.Provider;

public interface PermitsService extends Provider {
    void changePermit(UserModel userModel, String permitId);
    PermitResponse getCurrentPermit(UserModel userModel);
}
