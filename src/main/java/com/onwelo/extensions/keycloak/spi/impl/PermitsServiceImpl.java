package com.onwelo.extensions.keycloak.spi.impl;

import com.onwelo.extensions.keycloak.rest.PermitsRestResource;
import com.onwelo.extensions.keycloak.rest.dto.PermitResponse;
import com.onwelo.extensions.keycloak.spi.PermitsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.UserModel;

import java.util.Arrays;

public class PermitsServiceImpl implements PermitsService {

    private static final Log LOG = LogFactory.getLog(PermitsRestResource.class);
    private final String CURRENT_PERMIT_ID_KEY = "currentPermitId";

    private final KeycloakSession keycloakSession;

    public PermitsServiceImpl(KeycloakSession keycloakSession) {
        this.keycloakSession = keycloakSession;
    }

    public void changePermit(UserModel userModel, String permitId) {
        LOG.info("Setting permitId: " + permitId + " for current session");
        userModel.setAttribute(CURRENT_PERMIT_ID_KEY, Arrays.asList(permitId));
    }

    public PermitResponse getCurrentPermit(UserModel userModel) {
        LOG.info("Session attributes: " + userModel.getAttribute(CURRENT_PERMIT_ID_KEY));
        String currentPermitId = "default_permit_id";
        if (userModel.getAttribute(CURRENT_PERMIT_ID_KEY) != null && userModel.getAttribute(CURRENT_PERMIT_ID_KEY).size() > 0) {
            currentPermitId = userModel.getAttribute(CURRENT_PERMIT_ID_KEY).get(0);
        }
        PermitResponse permitResponse = new PermitResponse();
        permitResponse.setUsername(userModel.getUsername());
        permitResponse.setPermitId(currentPermitId);
        permitResponse.setSomeList(Arrays.asList("string_one_" + currentPermitId, "string_two_" + currentPermitId));

        return permitResponse;
    }

    public void close() {

    }
}
