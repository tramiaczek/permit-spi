package com.onwelo.extensions.keycloak.spi;

import org.keycloak.provider.ProviderFactory;

public interface PermitsServiceProviderFactory extends ProviderFactory<PermitsService> {
}
