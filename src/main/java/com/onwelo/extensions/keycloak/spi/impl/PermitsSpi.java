package com.onwelo.extensions.keycloak.spi.impl;

import com.onwelo.extensions.keycloak.spi.PermitsService;
import com.onwelo.extensions.keycloak.spi.PermitsServiceProviderFactory;
import org.keycloak.provider.Provider;
import org.keycloak.provider.ProviderFactory;
import org.keycloak.provider.Spi;

public class PermitsSpi implements Spi {

    public boolean isInternal() {
        return false;
    }

    public String getName() {
        return "permits";
    }

    public Class<? extends Provider> getProviderClass() {
        return PermitsService.class;
    }

    public Class<? extends ProviderFactory> getProviderFactoryClass() {
        return PermitsServiceProviderFactory.class;
    }
}
