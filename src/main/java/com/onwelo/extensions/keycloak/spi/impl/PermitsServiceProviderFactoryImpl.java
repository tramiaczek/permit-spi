package com.onwelo.extensions.keycloak.spi.impl;

import com.onwelo.extensions.keycloak.spi.PermitsService;
import com.onwelo.extensions.keycloak.spi.PermitsServiceProviderFactory;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class PermitsServiceProviderFactoryImpl implements PermitsServiceProviderFactory {
    public PermitsService create(KeycloakSession keycloakSession) {
        return new PermitsServiceImpl(keycloakSession);
    }

    public void init(Config.Scope scope) {

    }

    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {

    }

    public void close() {

    }

    public String getId() {
        return "permitServiceImpl";
    }
}
