package com.onwelo.extensions.keycloak.rest.dto;

import java.util.List;

public class PermitResponse {

    private String permitId;

    private String username;

    private List<String> someList;

    public PermitResponse() {
    }

    public String getPermitId() {
        return permitId;
    }

    public void setPermitId(String permitId) {
        this.permitId = permitId;
    }

    public List<String> getSomeList() {
        return someList;
    }

    public void setSomeList(List<String> someList) {
        this.someList = someList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
