package com.onwelo.extensions.keycloak.rest;

import com.onwelo.extensions.keycloak.rest.dto.PermitRequest;
import com.onwelo.extensions.keycloak.spi.PermitsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.services.managers.AppAuthManager;
import org.keycloak.services.managers.AuthenticationManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/")
public class PermitsRestResource {

    private static final Log LOG = LogFactory.getLog(PermitsRestResource.class);

    private final KeycloakSession keycloakSession;
    private final AuthenticationManager.AuthResult authResult;

    public PermitsRestResource(KeycloakSession keycloakSession) {
        this.keycloakSession = keycloakSession;
        this.authResult = new AppAuthManager().authenticateBearerToken(keycloakSession, keycloakSession.getContext().getRealm());
    }

    @POST
    @Consumes("application/json")
    public Response changePermitId(PermitRequest permitRequest) {
        verifyPermissions();
        PermitsService permitsService = this.keycloakSession.getProvider(PermitsService.class);
        permitsService.changePermit(this.authResult.getUser(), permitRequest.getPermitId());
        return Response.accepted().build();
    }

    @GET
    @Produces("application/json")
    public Response getCurrentPermit() {
        verifyPermissions();
        PermitsService permitsService = this.keycloakSession.getProvider(PermitsService.class);
        return Response.ok(permitsService.getCurrentPermit(this.authResult.getUser())).build();
    }

    private void verifyPermissions() {
        if (this.authResult == null) {
            throw new NotAuthorizedException("Bearer");
        } else if (this.authResult.getToken().getRealmAccess() == null || !this.authResult.getToken().getRealmAccess().isUserInRole("permit-change")) {
            throw new ForbiddenException("Does not have realm permit-change role");
        }

        LOG.info("Token: " + this.authResult.getToken().getIssuedFor());
    }
}
