package com.onwelo.extensions.keycloak.rest.dto;

public class PermitRequest {
    private String permitId;

    public PermitRequest() {
    }

    public PermitRequest(String permitId) {
        this.permitId = permitId;
    }

    public String getPermitId() {
        return permitId;
    }

    public void setPermitId(String permitId) {
        this.permitId = permitId;
    }
}
