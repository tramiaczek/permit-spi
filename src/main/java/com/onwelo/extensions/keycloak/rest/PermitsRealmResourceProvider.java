package com.onwelo.extensions.keycloak.rest;

import org.keycloak.models.KeycloakSession;
import org.keycloak.services.resource.RealmResourceProvider;

public class PermitsRealmResourceProvider implements RealmResourceProvider {

    private final KeycloakSession keycloakSession;

    public PermitsRealmResourceProvider(KeycloakSession keycloakSession) {
        this.keycloakSession = keycloakSession;
    }

    public Object getResource() {
        return new PermitsRestResource(this.keycloakSession);
    }

    public void close() {

    }
}
